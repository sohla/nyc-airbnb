import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import geopandas as gpd
import os
from shapely import wkt

print("new york airbnb data")
data = pd.read_csv("new-york-city-airbnb-open-data/AB_NYC_2019.csv")
#print(data)
print(data.dtypes)
sns.scatterplot(x="longitude",y="latitude", hue="neighbourhood_group",data=data)
plt.show()

d = data.pivot_table(index='room_type', columns='neighbourhood_group', values='price')
sns.heatmap(d)
plt.show()

"""
d = data.pivot_table(index='longitude', columns='latitude', values='price')
sns.heatmap(d)
plt.show()

sns.scatterplot(x="longitude",y="latitude", hue="price",data=data)
plt.show()
"""
